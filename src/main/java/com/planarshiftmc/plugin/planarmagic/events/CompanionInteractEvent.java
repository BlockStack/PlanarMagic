package com.planarshiftmc.plugin.planarmagic.events;

import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;

import com.planarshiftmc.plugin.planarmagic.companion.Companion;

/**
 * Triggered when a player interacts with a Companion.
 * 
 * @author wispoffates
 */
public class CompanionInteractEvent extends CompanionEvent {
	protected Action action;

	/**
	 * Constructor
	 * 
	 * @param companion
	 *                      The companion being interacted with.
	 * @param player
	 *                      The player interacting with the companion.
	 * @param action
	 *                      The action causing the interaction.
	 */
	public CompanionInteractEvent(Companion companion, Player player, Action action) {
		super(companion, player);
		this.action = action;
	}

	/**
	 * @return The action causing the interaction.
	 */
	public Action getAction() {
		return this.action;
	}

}
