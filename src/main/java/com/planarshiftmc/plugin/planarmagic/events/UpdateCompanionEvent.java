package com.planarshiftmc.plugin.planarmagic.events;

import org.bukkit.entity.Player;

import com.planarshiftmc.plugin.planarmagic.companion.Companion;

/**
 * Event triggered when updating a companion.
 * 
 * @author wispoffates
 */
public class UpdateCompanionEvent extends CompanionEvent {

	/**
	 * Constructor
	 * 
	 * @param companion
	 *                      The companion being removed.
	 * @param player
	 *                      The companion's player.
	 */
	public UpdateCompanionEvent(Companion companion, Player player) {
		super(companion, player);
	}

}
