package com.planarshiftmc.plugin.planarmagic.events;

import org.bukkit.entity.Player;

import com.planarshiftmc.plugin.planarmagic.companion.Companion;

/**
 * Event that is triggered when a companion removed.
 * 
 * @author wispoffates
 */
public class RemoveCompanionEvent extends CompanionEvent {

	/**
	 * Constructor
	 * 
	 * @param companion
	 *                      The companion being removed.
	 * @param player
	 *                      The companion's player.
	 */
	public RemoveCompanionEvent(Companion companion, Player player) {
		super(companion, player);
	}

}
