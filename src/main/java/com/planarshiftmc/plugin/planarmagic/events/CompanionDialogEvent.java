package com.planarshiftmc.plugin.planarmagic.events;

import java.util.Optional;

import org.bukkit.entity.Player;

import com.planarshiftmc.plugin.planarmagic.companion.Companion;

/**
 * Companion dialog is triggered on certain events and randomly for the
 * companion to say a line in chat.
 * 
 * @author wispoffates
 */
public class CompanionDialogEvent extends CompanionEvent {
	/**
	 * The message that will be sent to the player.
	 */
	public String msg;
	/**
	 * If set the message will be broadcast to all players within the radius.
	 */
	public Optional<Integer> radius = Optional.empty();

	/**
	 * Constructor
	 * 
	 * @param companion
	 *                      Companion doing the talking.
	 * @param player
	 *                      Player being talked too.
	 * @param message
	 *                      The message the player will see.
	 * @param radius
	 *                      If set the message will be broadcast to all players
	 *                      within range,
	 */
	public CompanionDialogEvent(Companion companion, Player player, String message, int radius) {
		super(companion, player);
		this.msg = message;
		this.radius = Optional.of(radius);
	}

	/**
	 * Constructor
	 * 
	 * @param companion
	 *                      Companion doing the talking.
	 * @param player
	 *                      Player being talked too.
	 * @param message
	 *                      The message the player will see.
	 */
	public CompanionDialogEvent(Companion companion, Player player, String message) {
		super(companion, player);
		this.msg = message;
	}

}
