package com.planarshiftmc.plugin.planarmagic;

import java.io.File;
import java.text.MessageFormat;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;

import com.comphenix.protocol.ProtocolManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.planarshiftmc.plugin.planarmagic.companion.Appearances;
import com.planarshiftmc.plugin.planarmagic.companion.Companions;
import com.planarshiftmc.plugin.planarmagic.gson.PostProcessingEnabler;
import com.planarshiftmc.plugin.planarmagic.photons.PhotonMappings;

/**
 * Planar Magic main plugin
 * 
 * @author wispoffates
 */
public class PlanarConduit extends JavaPlugin {

	private static PlanarConduit self;
	private static Appearances appearances;
	private static Companions companions;
	private static ProtocolManager protocolManager;
	private static PhotonMappings photonMappings;

	/**
	 * Retrieve the plugin instance.
	 * 
	 * @return The singleton instance.
	 */
	public static PlanarConduit self() {
		return self;
	}

	@Override
	public void onEnable() {
		self = this;
		this.loadConfig();
		// TODO: Figure out how to load defaults
		// attempt to update the config with newer values that might be missing.
		// this.getConfig().addDefaults(Configuration.);
		this.saveConfig(); // resave with the added defaults.
		appearances = new Appearances();
		companions = new Companions();

		// Register Commands
		CompanionCommandHandler commandHandler = new CompanionCommandHandler();
		this.getCommand("conduit").setExecutor(commandHandler);

		// Start timed tasks
		this.getServer().getScheduler().scheduleSyncRepeatingTask(this, new CompanionMovementTimedTask(), 0L, 1L);

		// Register listeners
		getServer().getPluginManager().registerEvents(new CompanionEventListener(), this);

		// cleanup companions left over from a crash
		Bukkit.getScheduler().runTaskLater(this, this::clearOutCrashedCompanions, 200);
		//this.clearOutCrashedCompanions();

		// load photon mappings
		photonMappings = new PhotonMappings();
	}

	private void clearOutCrashedCompanions() {
		this.getLogger().info("Starting scan for companions");
		long start = System.currentTimeMillis();
		int numberRemoved = 0;
		int numberScanned = 0;
		int numberWorlds = 0;
		for (World world : Bukkit.getWorlds()) {
			numberWorlds++;
			for (Entity ent : world.getEntities()) {
				numberScanned++;
				if (ent.getType() == EntityType.ARMOR_STAND) {
					if (ent.getScoreboardTags().contains("PlanarConduit")) {
						ent.remove();
						numberRemoved++;
					}
				}
			}
		}
		this.getLogger()
				.info(MessageFormat.format("Scanned {0} worlds with {1} entities and removed {2} companions in {3} ms.",
						numberWorlds, numberScanned, numberRemoved, (System.currentTimeMillis() - start)));
	}

	protected boolean loadConfig() {
		try {
			// make sure data folder is there
			if (!this.getDataFolder().exists()) {
				this.getDataFolder().mkdirs();
			}

			File cfgFile = new File(getDataFolder(), "config.yml");
			if (!cfgFile.exists()) {
				this.saveDefaultConfig();
			}
			this.getConfig().load(cfgFile);
			return true;
		} catch (Exception e) {
			this.getLogger().severe("Failed to load configuration! Please correct the error and use /reload to fix. :: "
					+ e.getMessage());
		}
		return false;
	}

	@Override
	public void onDisable() {
		HandlerList.unregisterAll(this);

		self = null;
		companions = null;
		appearances = null;
		// TODO remove all entities.
	}

	/**
	 * @return Get Appearances cache.
	 */
	public Appearances getAppearances() {
		return PlanarConduit.appearances;
	}

	/**
	 * @return Get Companions cache.
	 */
	public Companions getCompanions() {
		return PlanarConduit.companions;
	}

	/**
	 * @return Get Protocol Manager.
	 */
	public ProtocolManager getProtocol() {
		return PlanarConduit.protocolManager;
	}

	/**
	 * @return Get Photon Mappings.
	 */
	public PhotonMappings getPhotonMappings() {
		return photonMappings;
	}

	/**
	 * Build Gson object with type factories.
	 * 
	 * @return The Gson object.
	 */
	public static Gson getGson() {
		GsonBuilder builder = new GsonBuilder();
		builder.setPrettyPrinting();
		builder.registerTypeAdapterFactory(new PostProcessingEnabler());
		return builder.create();
	}

}
