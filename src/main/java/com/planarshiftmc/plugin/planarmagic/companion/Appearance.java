package com.planarshiftmc.plugin.planarmagic.companion;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;

import org.bukkit.entity.ArmorStand;
import org.bukkit.inventory.ItemStack;

/**
 * Represents a companions appearance.
 * 
 * @author wispoffates
 */
public class Appearance {
	private static final Random rand = new Random();

	protected String name;
	protected boolean areArmsVisible = true;
	protected AppearanceItem helmet;
	protected AppearanceItem chestplate;
	protected AppearanceItem legs;
	protected AppearanceItem boots;
	protected AppearanceItem mainhand;
	protected SIZE size = SIZE.SMALL;
	protected Map<DIALOG, List<String>> dialog = new HashMap<>();

	/**
	 * Enum for size of conduit.
	 */
	public enum SIZE {
		SMALL, SLARGE;
	}

	public enum DIALOG {
		IDLE, INTERACT, INTERACT_OTHER;
	}

	/**
	 * GSON Constructor
	 */
	protected Appearance() {
		// GSON will load this.
	}

	/**
	 * Constructor
	 * 
	 * @param name
	 *                 The name of the appearance.
	 */
	public Appearance(String name) {
		this.name = name;
	}


	/**
	 * Change appearance of an armor stand.
	 * 
	 * @param stand
	 *                  The armorstand to change appearance of.
	 * @return The armorstand with its appearance changed.
	 */
	public ArmorStand setAppearance(ArmorStand stand) {
		stand.setBasePlate(false);
		stand.setSmall(this.size == SIZE.SMALL);
		stand.setArms(this.areArmsVisible);
		// This makes it impossible to interact with an entity
		// Block the interaction in the event.
		// stand.setCanPickupItems(false);
		stand.setGravity(false);
		stand.setMarker(false);
		stand.setCollidable(false);
		stand.setVisible(false);
		stand.setCustomName(this.name);

		stand.setHelmet(this.createItemFromAppearance(this.helmet));
		stand.setChestplate(this.createItemFromAppearance(this.chestplate));
		stand.setLeggings(this.createItemFromAppearance(this.legs));
		stand.setBoots(this.createItemFromAppearance(this.boots));
		stand.setItemInHand(this.createItemFromAppearance(this.mainhand));
		return stand;
	}

	protected ItemStack createItemFromAppearance(AppearanceItem config) {
		if (config == null) {
			return null;
		}
		return config.getItem();
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * @param name
	 *                 the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the areArmsVisible
	 */
	public boolean isAreArmsVisible() {
		return this.areArmsVisible;
	}

	/**
	 * @param areArmsVisible
	 *                           the areArmsVisible to set
	 */
	public void setAreArmsVisible(boolean areArmsVisible) {
		this.areArmsVisible = areArmsVisible;
	}

	/**
	 * @return the helmet
	 */
	public AppearanceItem getHelmet() {
		return this.helmet;
	}

	/**
	 * @param helmet
	 *                   the helmet to set
	 */
	public void setHelmet(AppearanceItem helmet) {
		this.helmet = helmet;
	}

	/**
	 * @return the chestplate
	 */
	public AppearanceItem getChestplate() {
		return this.chestplate;
	}

	/**
	 * @param chestplate
	 *                       the chestplate to set
	 */
	public void setChestplate(AppearanceItem chestplate) {
		this.chestplate = chestplate;
	}

	/**
	 * @return the legs
	 */
	public AppearanceItem getLegs() {
		return this.legs;
	}

	/**
	 * @param legs
	 *                 the legs to set
	 */
	public void setLegs(AppearanceItem legs) {
		this.legs = legs;
	}

	/**
	 * @return the boots
	 */
	public AppearanceItem getBoots() {
		return this.boots;
	}

	/**
	 * @param boots
	 *                  the boots to set
	 */
	public void setBoots(AppearanceItem boots) {
		this.boots = boots;
	}

	/**
	 * @return the mainhand
	 */
	public AppearanceItem getMainhand() {
		return this.mainhand;
	}

	/**
	 * @param mainhand
	 *                     the mainhand to set
	 */
	public void setMainhand(AppearanceItem mainhand) {
		this.mainhand = mainhand;
	}

	/**
	 * @return the size
	 */
	public SIZE getSize() {
		return this.size;
	}

	/**
	 * @param size
	 *                 the size to set
	 */
	public void setSize(SIZE size) {
		this.size = size;
	}

	/**
	 * Retrieve random dialog from the group.
	 * 
	 * @param key
	 *                The dialog group.
	 * @return Optionally the dialog requested.
	 */
	public Optional<String> getDialog(DIALOG key) {
		if (this.dialog.containsKey(key)) {
			int val = rand.nextInt(this.dialog.get(key).size());
			return Optional.ofNullable(this.dialog.get(key).get(val));
		}
		return Optional.empty();
	}

	/**
	 * Add dialog for this sessions.
	 * 
	 * @param key
	 *                   The group of the dialog.
	 * @param dialog
	 *                   The dialog to add.
	 */
	public void addDialog(DIALOG key, String dialog) {
		if (this.dialog.containsKey(key)) {
			this.dialog.get(key).add(dialog);
		} else {
			LinkedList<String> dialogs = new LinkedList<>();
			dialogs.add(dialog);
			this.dialog.put(key, dialogs);
		}
	}

	/**
	 * Remove particular dialog for this session.
	 * 
	 * @param key
	 *                   The group of the dialog.
	 * @param dialog
	 *                   The dialog to remove
	 */
	public void remmoveDialog(DIALOG key, String dialog) {
		if (this.dialog.containsKey(key)) {
			this.dialog.get(key).remove(dialog);
		}
	}

	/**
	 * Clear all dialog for a given key.
	 * 
	 * @param key
	 *                The key of the dialog to remove.
	 */
	public void clearDialog(DIALOG key) {
		this.dialog.remove(key);
	}

}
