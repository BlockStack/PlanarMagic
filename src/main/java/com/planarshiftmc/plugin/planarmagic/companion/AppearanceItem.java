package com.planarshiftmc.plugin.planarmagic.companion;

import java.util.Base64;
import java.util.UUID;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import com.mojang.authlib.properties.PropertyMap;
import com.planarshiftmc.plugin.planarmagic.skull.Reflections;


/**
 * Represents an item that is attached to the armor stand
 * 
 * @author wispoffates
 */
public class AppearanceItem {

	protected Material item;
	protected String url;
	protected Color color;
	protected boolean glow;

	/**
	 * Gson Constructor
	 */
	public AppearanceItem() {

	}

	/**
	 * Constructor
	 * 
	 * @param mat
	 *                Material of the item.
	 */
	public AppearanceItem(Material mat) {
		this.item = mat;
	}

	public ItemStack getItem() {
		if (this.item == Material.PLAYER_HEAD && this.url != null) {
			return getCustomSkull(this.url);
		}
		ItemStack stack = new ItemStack(this.item);
		if (this.color != null) {
			ItemMeta meta = stack.getItemMeta();
			if (meta instanceof LeatherArmorMeta) {
				LeatherArmorMeta.class.cast(meta).setColor(color);
				stack.setItemMeta(meta);
			}
		}
		if (this.glow) {
			ItemMeta meta = stack.getItemMeta();
			meta.addEnchant(Enchantment.LURE, 0, false);
			stack.setItemMeta(meta);
		}
		return stack;
	}

	private static ItemStack getCustomSkull(String url) {
		GameProfile profile = new GameProfile(UUID.randomUUID(), null);
		PropertyMap propertyMap = profile.getProperties();
		if (propertyMap == null) {
			throw new IllegalStateException("Profile doesn't contain a property map");
		}
		byte[] encodedData = Base64.getEncoder()
				.encode(String.format("{textures:{SKIN:{url:\"%s\"}}}", url).getBytes());
		propertyMap.put("textures", new Property("textures", new String(encodedData)));
		ItemStack head = new ItemStack(Material.PLAYER_HEAD, 1);
		ItemMeta headMeta = head.getItemMeta();
		Class<?> headMetaClass = headMeta.getClass();
		Reflections.getField(headMetaClass, "profile", GameProfile.class).set(headMeta, profile);

		head.setItemMeta(headMeta);

		return head;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url
	 *                the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the color
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * @param color
	 *                  the color to set
	 */
	public void setColor(Color color) {
		this.color = color;
	}

	/**
	 * @return the glow
	 */
	public boolean isGlow() {
		return glow;
	}

	/**
	 * @param glow
	 *                 the glow to set
	 */
	public void setGlow(boolean glow) {
		this.glow = glow;
	}

	/**
	 * @param item
	 *                 the item to set
	 */
	public void setItem(Material item) {
		this.item = item;
	}
}
