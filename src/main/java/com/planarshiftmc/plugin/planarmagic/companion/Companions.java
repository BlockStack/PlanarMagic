package com.planarshiftmc.plugin.planarmagic.companion;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.planarshiftmc.plugin.planarmagic.PlanarConduit;

/**
 * Retrieves companions for players.
 * 
 * @author wispoffates
 */
public class Companions {
	protected Map<UUID, Companion> companions;
	protected Map<Integer, Companion> companionsByEntId;

	/**
	 * Constructor
	 */
	public Companions() {
		this.companions = new HashMap<>();
		this.companionsByEntId = new HashMap<>();
	}

	/**
	 * Retrieve the player's companion.
	 * 
	 * @param player
	 *                   The player.
	 * @return The player's companion.
	 */
	public Companion getCompanion(UUID player) {
		if (!this.companions.containsKey(player)) {
			this.companions.put(player, this.loadCompanion(player));
		}
		return this.companions.get(player);
	}

	/**
	 * Retrieve companion associated with the provided player.
	 * 
	 * @param player
	 *                   The player.
	 * @return The companion associated with the player.
	 */
	public Companion getCompanion(Player player) {
		return this.getCompanion(player.getUniqueId());
	}

	/**
	 * Retrieve companion by its entity ID.
	 * 
	 * @param entId
	 *                  The id of the entity to find.
	 * @return The entity if found and null otherwise.
	 */
	public Companion getCompanion(int entId) {
		return this.companionsByEntId.get(entId);
	}

	/**
	 * Retrieve iterator over companions.
	 * 
	 * @return The iterator.
	 */
	public Iterator<Companion> getCompanions() {
		return this.companions.values().iterator();
	}

	/**
	 * Remove the companion for the provided player
	 * 
	 * @param player
	 *                   The owner of the companion to remove.
	 */
	public void removeCompanion(UUID player) {
		this.companions.get(player).remove(); // despawn companion
		Companion comp = this.companions.remove(player);
		this.companionsByEntId.remove(comp.enityId);
	}

	/**
	 * Remove all companions.
	 */
	public void removeAllCompanions() {
		for (Companion companion : this.companions.values()) {
			companion.remove();
		}
		this.companions.clear();
	}

	/**
	 * Used to link a companion to an entity ID.
	 * 
	 * @param companion
	 *                      The companion to update.
	 */
	public void updateCompanion(Companion companion) {
		this.companionsByEntId.put(companion.enityId, companion);
		this.saveCompanion(companion);
	}

	/**
	 * Load companion file or create a new one if it does not exist.
	 * 
	 * @param player
	 *                   The player the companion is for.
	 * @return The created or loaded companion.
	 */
	protected Companion loadCompanion(UUID player) {
		Companion companion = new Companion(Bukkit.getPlayer(player));
		File companionsFolder = new File(PlanarConduit.self().getDataFolder(), "companions");
		if (!companionsFolder.exists()) {
			companionsFolder.mkdirs();
		}
		File companionFile = new File(companionsFolder, player.toString() + ".json");
		try {
			companion = PlanarConduit.getGson().fromJson(new FileReader(companionFile), Companion.class);
		} catch (JsonIOException | JsonSyntaxException e) {
			PlanarConduit.self().getLogger().log(Level.SEVERE,
					"Companion file damaged backing up to .corrupt and loading default companion.", e);
			companionFile.renameTo(new File(companionFile.getParentFile(), player.toString() + ".corrupt"));
		} catch (FileNotFoundException e) {
			// will happen on first login
		}
		this.companions.put(player, companion);
		return companion;
	}

	protected void saveCompanion(Companion companion) {
		File companionsFolder = new File(PlanarConduit.self().getDataFolder(), "companions");
		if (!companionsFolder.exists()) {
			companionsFolder.mkdirs();
		}
		File companionFile = new File(companionsFolder, companion.getPlayer().getUniqueId().toString() + ".json");
		try (FileWriter writer = new FileWriter(companionFile)) {
			PlanarConduit.getGson().toJson(companion, writer);
			writer.flush();
			writer.close();
		} catch (JsonIOException | IOException e) {
			PlanarConduit.self().getLogger().log(Level.SEVERE,
					"Companion file could not be written to " + companion.toString() + "!", e);
		}
	}

}
