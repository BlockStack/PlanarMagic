package com.planarshiftmc.plugin.planarmagic.electrolyser;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Event.Result;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.planarshiftmc.plugin.planarmagic.PlanarConduit;

/**
 * Interface for turning item into photons
 * 
 * @author wispoffates
 */
public class Electrolyser implements Listener {

	protected Inventory brewInv;
	protected Player player;

	enum SLOT {
		POTION_LEFT(0), POTION_CENTER(1), POTION_RIGHT(2), INPUT(3), SIGIL(4);

		public int slotID;

		private SLOT(int id) {
			this.slotID = id;
		}

		public static SLOT fromId(int id) {
			for (SLOT slot : values()) {
				if (slot.slotID == id) {
					return slot;
				}
			}
			throw new IllegalArgumentException("Invalid slot id: " + id);
		}
	}

	public Electrolyser(Player player) {
		this.player = player;
		// this.brewInv = Bukkit.createInventory(player, InventoryType.BREWING);
		this.brewInv = PlanarConduit.self().getServer().createInventory(player, InventoryType.BREWING, "Electrolyser");
		this.brewInv.setItem(0, new ItemStack(Material.ENDER_PEARL));
		this.brewInv.setItem(1, new ItemStack(Material.ENDER_PEARL));
		this.brewInv.setItem(2, new ItemStack(Material.ENDER_PEARL));
	}

	public void open() {
		this.player.openInventory(this.brewInv);
		PlanarConduit.self().getServer().getPluginManager().registerEvents(this, PlanarConduit.self());
	}

	@EventHandler
	public void onInventoryClickEvent(InventoryClickEvent event) {
		if (event.getClickedInventory().equals(this.brewInv)) {
			// this.player.sendMessage(MessageFormat.format("Clicked slot {0} which contains
			// {1} placing {2}.",event.getSlot(), event.getCurrentItem(),
			// event.getCursor()));
			// cancel if the potions are clicked they are placeholders for batteries
			// They can't be removed until photons are stored
			SLOT slot = SLOT.fromId(event.getSlot());
			switch (slot) {
			case POTION_LEFT:
			case POTION_CENTER:
			case POTION_RIGHT: {
				// do nothing with these right now
				event.setCancelled(true);
				return;
			}
			case SIGIL: {
				event.setCancelled(true);
				event.getWhoClicked().sendMessage("Touched sigil position");
				return;
			}
			case INPUT:
				if (event.getCursor() != null) {
					try {
						int amount = PlanarConduit.self().getPhotonMappings().getPhotons(event.getCursor());
						if (amount < 1) {
							return; // should trigger the catch fist but makes linter happy.
						}
						event.setCurrentItem(event.getCursor());
						event.setResult(Result.ALLOW);
						this.player.setItemOnCursor(null);
					} catch (Exception e) {
						this.player.sendMessage("Cannot Electrolyse :: " + e);
					}
				}
			}
		}
	}

	@EventHandler
	public void onInventoryCloseEvent(InventoryCloseEvent event) {
		if (event.getInventory().equals(this.brewInv)) {
			HandlerList.unregisterAll(this);
		}
	}

}
