package com.planarshiftmc.plugin.planarmagic;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerJoinEvent;

import com.planarshiftmc.plugin.planarmagic.companion.Appearance.DIALOG;
import com.planarshiftmc.plugin.planarmagic.companion.Companion;
import com.planarshiftmc.plugin.planarmagic.electrolyser.Electrolyser;
import com.planarshiftmc.plugin.planarmagic.events.CompanionDialogEvent;
import com.planarshiftmc.plugin.planarmagic.events.CompanionInteractEvent;

/**
 * Main event handler.
 * 
 * @author wuspoffates
 */
public class CompanionEventListener implements Listener {

	/**
	 * On player login load their attunements.
	 * 
	 * @param event
	 *                  The login event.
	 */
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		// Spawn the companion
		Companion companion = PlanarConduit.self().getCompanions().getCompanion(event.getPlayer());
		companion.spawn();
	}

	@EventHandler(priority = EventPriority.LOWEST) // Lowest means first
	public void onPlayerInteractEnityEvent(PlayerInteractEntityEvent event) {
		if (event.getRightClicked().getType() != EntityType.ARMOR_STAND) {
			return; // not an armor stand
		}
		Companion companion = PlanarConduit.self().getCompanions().getCompanion(event.getRightClicked().getEntityId());
		if (companion != null) {
			// call the CompanionInteractEvent
			event.setCancelled(true);
			Bukkit.getServer().getPluginManager()
					.callEvent(new CompanionInteractEvent(companion, event.getPlayer(), Action.RIGHT_CLICK_BLOCK));
		}

	}

	@EventHandler(priority = EventPriority.LOWEST) // Lowest means first
	public void onPlayerInteractAtEnityEvent(PlayerInteractAtEntityEvent event) {
		if (event.getRightClicked().getType() != EntityType.ARMOR_STAND) {
			return; // not an armor stand
		}
		Companion companion = PlanarConduit.self().getCompanions().getCompanion(event.getRightClicked().getEntityId());
		if (companion != null) {
			// call the CompanionInteractEvent
			event.setCancelled(true);
			CompanionInteractEvent cie = new CompanionInteractEvent(companion, event.getPlayer(),
					Action.RIGHT_CLICK_BLOCK);
			Bukkit.getServer().getPluginManager().callEvent(cie);
			if (!cie.isCancelled()) {
				CompanionDialogEvent cde = new CompanionDialogEvent(companion, event.getPlayer(), "");
				if (companion.getPlayer().equals(event.getPlayer())) {
					cde.msg = companion.getDialog(DIALOG.INTERACT).orElse("No Dialog set!");
				} else {
					cde.msg = companion.getDialog(DIALOG.INTERACT_OTHER).orElse("No Dialot set!");
				}
				Bukkit.getServer().getPluginManager().callEvent(cde);
				if(!cde.isCancelled()) {
					if(cde.radius.isPresent()) {
						List<Entity> entities = companion.getEntity().getNearbyEntities(cde.radius.get(),
								cde.radius.get(), cde.radius.get());
						for (Entity ent : entities) {
							ent.sendMessage(cde.msg);
						}
					} else {
						event.getPlayer().sendMessage(cde.msg);
					}
				} else {
					return;
				}
				Electrolyser electro = new Electrolyser(event.getPlayer());
				electro.open();
			}
		}

	}

	/*
	 * ------------Companion Events -----------------------------------------
	 */


}
